
var express = require('express');
var app = express();
var server = app.listen(3000);

// app.use('public', express.static(__dirname + 'public'));
app.use(express.static('public'));

console.log("Server running on 3000...");

var socket = require('socket.io');
var io = socket(server);

io.sockets.on('connection', newConnection);

function newConnection(socket) {
  console.log('new connection: ' + socket.id);

  socket.on('mouse', mouseMessage);

  function mouseMessage(data) {
    socket.broadcast.emit('mouse', data); // emits to everyone excluding this client
    // io.sockets.emit('mouse', data); // emits to everyone including this client
    console.log(data);
  }
}
